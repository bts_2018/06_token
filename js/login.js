$( document ).ready(function() {
    
        $('#login-form').submit( processForm );

        function processForm(e) {
            e.preventDefault(); 
            var username = $("#login-form").find("input[name='username']").val();
            var password = $("#login-form").find("input[name='password']").val();    
            
            if(username != '' && password != ''){
                $.ajax({
                    dataType: 'json',
                    type:'GET',
                    url: 'http://www.winelove.club/heroes2/login.php',
                    data:{username:username, password:password}         
                }).done(function(data){
                
                    //save session storage
                    if (data["token"] != "error") {                                                
                        sessionStorage.setItem('token', JSON.stringify(data["token"]));                                                                           
                        window.location.href="heroes.html";
                    } else {
                       $("#loginError").show();
                    }
                });        
            }
        }
        
        /* Create new Item */            
    
        $( "#username" ).keypress(function() {
          $("#loginError").hide();
        });
    
    
        $( "#password" ).keypress(function() {
          $("#loginError").hide();
        });       

});