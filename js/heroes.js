$(document).ready(function() { 
   
    var server = "http://www.winelove.club";       
    var token;

    checkSession();
    
    addEventListener();

    loadData();
    
    /************************* NEW HERO **********************************/
    function processForm(e) {
        e.preventDefault(); //stop the browser;
        
        var newHero = $("#newHero").val(); //get the newHero value from the create form
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/heroes2/create.php',
            data:{name:newHero,token:token}
        }).done(function(heroes){

            $("#newHero").val(''); //clean the input value
            $(".modal").modal('hide'); //close the modal page
            showtoast("add",function() {
                manageRow(heroes); //callback
            });

        });  
    }

    /************************* EDIT HERO **********************************/
    function EditForm(e) {
        e.preventDefault();
        
        //get elements from the form
        var idHero = $("#idHero").val();  
        var nameHero = $("#editHero").val();

        //ajax call
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/heroes2/update.php',
            data:{id:idHero,name:nameHero,token:token}
        }).done(function(heroes){
            $("#editHero").val(); //clean the input value
            $(".modal").modal('hide'); //close the modal page
            showtoast("edit",function() {
                manageRow(heroes); //callback
            });
        });  

    }

    /************************* SELECT HERO *********************************/
    function selectedItem() {        
        var idHero = $(this).parent("td").data("id"); //selected ID
        var nameHero = $(this).closest('tr').children('td:eq(1)').text(); //get the name Hero from the select item
    
        $("#idHero").val(idHero); //set the idHero inside the edit Form
        $("#editHero").val(nameHero); //set the input text inside the edit Form
    }

    /************************* REMOVE HERO *********************************/ 
    function removeItem() {
         if (confirm("are you sure to delete ?")) {
            var idHero = $(this).parent("td").data("id");
            
            $.ajax({
                dataType: 'json',
                type:'GET',
                url: server + '/heroes2/delete.php',
                data:{id:idHero,token:token}
            }).done(function(heroes){

                showtoast("remove",function() {
                    manageRow(heroes); //callback
                });

            });  
        }

    }



    /************************* functions **********************************/
    function showtoast(action,callback){
        
        switch (action) {
            case "remove":
                $.toast({ heading: 'Remove', text: 'Hero has been removed', icon: 'success',hideAfter:1000})
            break; 

            case "add":
                $.toast({ heading: 'Add', text: 'Hero has been added', icon: 'success', hideAfter:1000})
            break; 

            case "edit":
                $.toast({ heading: 'Edit', text: 'Hero has been edited', icon: 'success', hideAfter:1000})
            break; 

            case  "itemnotfound":
                $.toast({ heading: 'item not found', text: 'item not found', icon: 'success', hideAfter:1000})
            break; 

            case "search":
            $.toast({ heading: 'Search', text: 'Search....', icon: 'success', hideAfter:1000})
            break; 

            case "showAll":
            $.toast({ heading: 'Search', text: 'Search....', icon: 'success', hideAfter:1000})
            break; 

            case "error":
                $.toast({heading: 'Error ', text: 'Impossible to connect... ', icon: 'error',hideAfter:1000})
            break; 
        }   

        setTimeout(function(){
            callback();
        }, 1000); // after 1 second
    }

    function manageRow(heroes){
        $("tbody").html(''); //clean the list

        $.each( heroes, function( key, value ) {
            var	row;
            row = '<tr>';
                row = row + '<td >'+value.id+'</td>';
                row = row + '<td>'+value.name+'</td>';
                row = row + '<td data-id="' + value.id + '">';
                    row = row + '<button  data-toggle="modal" data-target="#edit-item"  class="btn btn-primary edit-item">Edit</button> ';
                    row = row + '<button  class="btn btn-danger remove-item">Delete</button></td>';
                row = row + '</td>';
            row = row + '</tr>';

            $("tbody").append(row);
        });
    }
    
    function SearchForm(event){
        event.preventDefault(); //stop the browser;
        var searchHero = $("#searchHero").val(); 

        $.ajax({
            dataType: 'json',
            type:'GET',
            url: server + '/heroes2/search.php',
            data:{name:searchHero,token:token}
        }).done(function(heroes){
            console.log(heroes);            
            if (heroes.error == "item not found."){
                showtoast("itemnotfound",function() {
                    $("tbody").html(''); //clean the list
                    $("#showAllButton").show();
                });
            }else{
                showtoast("search",function() {
                    manageRow(heroes); //callback                  
                    $("#showAllButton").show();
                });        
            }

            

        });  
    }

    function showAll(){
        $.ajax({
            dataType: 'json',
            url: server + '/heroes2/read.php',
            data:{token:token}
        }).done(function(heroes){
            showtoast("showall",function() {
                manageRow(heroes);    
                $("#showAllButton").hide();
                $("#searchHero").val('');
            });
                
        });
    }

    function checkSession(){
        token = JSON.parse(sessionStorage.getItem('token'));     
        console.log ("token:" +  JSON.stringify(token));
        if (token == undefined) {
            window.location.href="login.html";
        }
    }
  
    function addEventListener(){
       
        //form events
        $('#formCreate').submit( processForm );
        $('#formEdit').submit( EditForm );
        $('#searchForm').submit( SearchForm );

        //button events
        $("#showAllButton").on("click",showAll);
        $("#logout").on("click",logout);

        // row events
        $("#tablelist").on("click",".edit-item",selectedItem);  
        $("#tablelist").on("click",".remove-item",removeItem); 
    }

    function loadData (){
    
        $("#showAllButton").hide(); //hide the show all button
        
        $.ajax({
            dataType: 'json',
            url: server + '/heroes2/read.php',
            data:{token:token}
            }).done(function(heroes){
                manageRow(heroes);        
            }).fail(function(errror){
                showtoast('error');
        });
    
    }

    function logout(){
        sessionStorage.removeItem('token');
        window.location.href="login.html";
    }


});